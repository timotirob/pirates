<?php
/**
 * Created by PhpStorm.
 * User: Timothee
 * Date: 27/09/2019
 * Time: 10:13
 */
// Your custom class dir

require "autoload.php";

//echo "bonjour";

//echo Marin.class::quiSuisJe();
$gentilMarin = new pirates\Marin("Dupond", "artilleur") ;
//echo "$gentilMarin" ;

$robert = new pirates\Capitaine("Robert", "prof", "senior") ;
//$robert->commande() ;

//echo "$robert" ;

$ackab = new pirates\Marin("Ackab", "mousse") ;

$unEquipage = array($gentilMarin,$robert,$ackab) ;
$navire = new pirates\Navire($unEquipage,26,"Corvette") ;

echo "$navire" ;

$pirate = new pirates\Pirate("Le Rouge", "Cannonnier", 32) ;

$pirate->pillage($navire) ;

echo "$navire" ;