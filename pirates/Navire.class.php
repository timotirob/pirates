<?php
/**
 * Created by PhpStorm.
 * User: Timothee
 * Date: 27/09/2019
 * Time: 14:00
 */

namespace pirates ;


class Navire {
    private $equipage = array() ;
    private $taille ;
    private $modele ;

    /**
     * Navire.class constructor.
     * @param $equipage
     * @param $taille
     * @param $modele
     */
    public function __construct($equipage, $taille, $modele)
    {
        $this->equipage = $equipage;
        $this->taille = $taille;
        $this->modele = $modele;
    }

    /**
     * @return mixed
     */
    public function getEquipage()
    {
        return $this->equipage;
    }

    /**
     * @param mixed $equipage
     */
    public function setEquipage($equipage)
    {
        $this->equipage = $equipage;
    }

    /**
     * @return mixed
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * @param mixed $taille
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;
    }

    /**
     * @return mixed
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * @param mixed $modele
     */
    public function setModele($modele)
    {
        $this->modele = $modele;
    }

    public function ajouteMarin($unMarin) {
        array_push($this->equipage,$unMarin) ;

    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        $aRetourner = "Le navire est de taille ".$this->getTaille()." et son modèle est: ".$this->getModele()."<BR>";

        foreach ($this->getEquipage() as $unMarin){
            $aRetourner.=$unMarin ;
    }
    return $aRetourner ;

    }


}