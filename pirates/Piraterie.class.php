<?php

namespace pirates ;


interface Piraterie {
    public function pillage (Navire $unNavire);
}