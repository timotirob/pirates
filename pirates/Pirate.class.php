<?php

namespace pirates ;



class Pirate extends Marin implements Piraterie
{
    private $degreSauvagerie ;

    /**
     * Pirate.class constructor.
     * @param $degreSauvagerie
     */
    public function __construct($nom, $fonction, $degreSauvagerie)
    {
        parent::__construct($nom,$fonction);
        $this->degreSauvagerie = $degreSauvagerie;
    }

    public function pillage(Navire $unNavire)
    {
        // TODO: Implement pillage() method.
        /**
        if ($this->degreSauvagerie > 50) {
            $equipageVide = array();
            $unNavire->setEquipage($equipageVide);
        }
         *
         * */

        if ($this->degreSauvagerie > 50) {
            $equipageVide = array();
            $unNavire->setEquipage($equipageVide);
        }
        else {
            $newEquipage = array() ;
            foreach ($unNavire->getEquipage() as $unMarin)
            {
                if ($unMarin instanceof Capitaine)
                {

                }
                else {
                    array_push($newEquipage,$unMarin) ;
                }
            }
            $unNavire->setEquipage($newEquipage);

        }

    }

    /**
     * @return mixed
     */
    public function getDegreSauvagerie()
    {
        return $this->degreSauvagerie;
    }

    /**
     * @param mixed $degreSauvagerie
     */
    public function setDegreSauvagerie($degreSauvagerie)
    {
        $this->degreSauvagerie = $degreSauvagerie;
    }




}